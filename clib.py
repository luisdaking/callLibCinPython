from ctypes import *
import time


# libc = cdll.LoadLibrary("libc.so.6")
# libc.printf.argtypes = [c_char_p, c_char_p, c_int, c_double]
# libc.printf(b"String '%s', Int %d, Double %f\n", b"Hi", 10, 2.2)


def sumaC(a: float, b: float):
    lib = cdll.LoadLibrary("libmylib.so")
    lib.suma.argtypes = [c_float, c_float]
    lib.suma.restype = c_float
    return lib.suma(a, b)

print(sumaC(3.0, 3.2))
